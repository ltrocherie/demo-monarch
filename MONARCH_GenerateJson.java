import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;
import java.util.Calendar; 
import java.util.regex.Pattern;
import java.util.regex.Matcher;
import java.io.*;

public class MONARCH_GenerateJson {

    private static String archunitStoreFolderPath;
    private static String key;
    private static String branch;
    private static String jsonStoreLocation;
    private static String json;

    private static HashMap<String,String> getArchunitRulesFiles() throws IOException,FileNotFoundException{
        HashMap<String,String> files = new HashMap<String,String>();
        File file=new File(archunitStoreFolderPath + "/stored.rules");
        FileReader fr=new FileReader(file);
        BufferedReader br = new BufferedReader(fr);
        String line;
        while((line=br.readLine())!=null)  
        {  
            if(line.contains("=")){ // Ignores the first lines with no data
                String[] elements = line.split("=");
                files.put(elements[0],elements[1]);
            }
        }  
        fr.close();
        return files;
    }

    private static List<String> getListOfBrokenRules(String fileName) throws IOException, FileNotFoundException{
        List<String> brokenRules = new ArrayList<String>();
        File file=new File(archunitStoreFolderPath + "/" + fileName);
        FileReader fr=new FileReader(file);
        BufferedReader br = new BufferedReader(fr);
        String line;
        while((line=br.readLine())!=null)  
        {  
            brokenRules.add(line);
        }  
        fr.close();
        return brokenRules;
    }

    private static String getCurrentTimeStamp(){
        Calendar c = Calendar.getInstance();
        String tempMonth = "0" + (c.get(Calendar.MONTH) + 1);
        String tempDay = "0" + c.get(Calendar.DAY_OF_MONTH);
        String tempHour = "0" + c.get(Calendar.HOUR_OF_DAY);
        String tempMinute = "0" + c.get(Calendar.MINUTE);
        String tempSecond = "0" + c.get(Calendar.SECOND);
        return c.get(Calendar.YEAR)
             + "-"
             + tempMonth.substring(tempMonth.length() - 2)
             + "-"
             + tempDay.substring(tempDay.length() - 2)
             + "T"
             + tempHour.substring(tempHour.length() - 2)
             + ":"
             + tempMinute.substring(tempMinute.length() - 2)
             + ":"
             + tempSecond.substring(tempSecond.length() - 2);
    }

    private static String getErrorPosition(String error){
        String[] parsedError = error.split(" ");
        return (parsedError[parsedError.length - 1]).substring(1,parsedError[parsedError.length - 1].length()-1);
    }

    private static HashMap<String,List<String>> sortErrorsByPackage(List<String> rules){
        HashMap<String,List<String>> packageMap = new HashMap<String,List<String>>();
        for(String rule:rules){
            String fileName = getErrorPosition(rule).split(":")[0];
            String classFileName = fileName.substring(0,fileName.length() - ".java".length());
            Pattern pattern = Pattern.compile(classFileName);
            Matcher matcher = pattern.matcher(rule); 
            while (matcher.find()) {
                if(rule.charAt(matcher.start()-1) == '.'){
                    int index = matcher.start() - 1;
                    while((index >= 0) && (rule.charAt(index) != ' ' && rule.charAt(index) != '<')) // Finding the begining of the string containing the package name 
                        index --;
                    String packageName = rule.substring(index + 1, matcher.start()-1);
                    if(packageMap.containsKey(packageName)){
                        List<String> lst = packageMap.get(packageName);
                        lst.add(rule);
                        packageMap.put(packageName, lst);
                    }
                    else{
                        List<String> lst = new ArrayList<>();
                        lst.add(rule);
                        packageMap.put(packageName,lst);
                    }
                }
            }
        }
        return packageMap;
    }


    private static void writeToFile() throws IOException{
        File file=new File(jsonStoreLocation);
        FileWriter fw=new FileWriter(file);
        fw.write(json);
        fw.close();
    }

    public static void main(String[] args) {
        if(args.length > 1){
            archunitStoreFolderPath = args[0];
            key = args[1];
            branch = args[2];
            System.out.println("Parsing the archunit-store rules at: " + archunitStoreFolderPath);
            jsonStoreLocation = archunitStoreFolderPath + "/archunit-store.json";
            try {
                json = "{ " +
                "\"key\": \"" + key + "-" + branch + "\","+
                "\"timestamp\": \"" + getCurrentTimeStamp() + "\","+
                "\"report\": [";
                HashMap<String,String> archunitRulesFiles = getArchunitRulesFiles();
                for (String ruleName : archunitRulesFiles.keySet()) {
                    List<String> brokenRules = getListOfBrokenRules(archunitRulesFiles.get(ruleName));
                    if(brokenRules.size() > 0){
                        HashMap<String,List<String>> packageErrors = sortErrorsByPackage(brokenRules);
                        json += "{"+ 
                                "\"rule\": \"" + ruleName.replace("\\","") + "\","+
                                "\"errors\":[";
                        for(String packageName : packageErrors.keySet()){
                            List<String> errors = packageErrors.get(packageName);
                            json += "{"+
                                    "\"packageName\":\""+ packageName + "\","+
                                    "\"nbErrors\":" + errors.size() + "," +
                                    "\"errors\":[";
                                for(String error:errors){
                                    json += "{" +
                                            "\"error\":\"" + error + "\","+
                                            "\"position\":\"" + getErrorPosition(error) + "\""+
                                            "},";
                                }
                                json = json.substring(0,json.length()-1);
                            json += "]},";
                        }
                        json = json.substring(0,json.length()-1);
                        json += "]},";
                    }
                } 
                json = json.substring(0,json.length()-1);
                json += "]}";
                writeToFile();
                System.out.println("---------------------------------------------------------");
                System.out.println(json);
                System.out.println("---------------------------------------------------------");

            } 
            catch (FileNotFoundException e) {
                System.out.println("File not found in folder " + archunitStoreFolderPath);
                System.exit(1);
            }
            catch(IOException e){
                System.out.println("Error while reading the files in folder " + archunitStoreFolderPath);
                System.exit(1);
            }
            
        }
        else{
            System.out.println("Usage : java -ea GenerateJson <Path To Archunit Store Folder From Generate Json Folder> <key name> <branch name>");
            System.exit(1);
        }
    }
}