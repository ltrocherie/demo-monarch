package architecture;

import architecture.configuration.*;
import com.soprasteria.architecture.SetupConfiguration;
import com.soprasteria.architecture.layer.ILayerArchitectureConfiguration;
import com.soprasteria.architecture.layer.LayerArchitectureRules;
import com.soprasteria.architecture.location.ILocationArchitectureConfiguration;
import com.soprasteria.architecture.location.LocationArchitectureRules;
import com.soprasteria.architecture.naming.INamingArchitectureConfiguration;
import com.soprasteria.architecture.naming.NamingArchitectureRules;
import com.soprasteria.architecture.misc.dao.DAOArchitectureRules;
import com.soprasteria.architecture.misc.dao.IDAOArchitectureConfiguration;
import com.soprasteria.architecture.misc.interfaces.IInterfaceArchitectureConfiguration;
import com.soprasteria.architecture.misc.interfaces.InterfaceArchitecturesRules;
import com.tngtech.archunit.core.importer.ImportOption;
import com.tngtech.archunit.junit.AnalyzeClasses;
import com.tngtech.archunit.junit.ArchRules;
import com.tngtech.archunit.junit.ArchTest;
import com.tngtech.archunit.junit.ArchUnitRunner;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;

@RunWith(ArchUnitRunner.class)
@AnalyzeClasses(packages = "tec", importOptions = ImportOption.DoNotIncludeTests.class)
public class ArchitectureAutobusTest {
    /* All the configuration available as defined bellow in the README */
    static ILayerArchitectureConfiguration layerArchitectureConfiguration = new LayerArchitectureConfiguration();
    static INamingArchitectureConfiguration namingArchitectureConfiguration = new NamingArchitectureConfiguration();
    static ILocationArchitectureConfiguration locationArchitectureConfiguration = new LocationArchitectureConfiguration();
    static IInterfaceArchitectureConfiguration interfaceArchitectureConfiguration = new InterfaceArchitectureConfiguration();

    @BeforeClass
    public static void setUpConfigurationVariable() {

        /* Associate all the implementation of configuration at each rules class */
        LayerArchitectureRules.layerArchitectureConfiguration = layerArchitectureConfiguration;
        NamingArchitectureRules.namingArchitectureConfiguration = namingArchitectureConfiguration;
        LocationArchitectureRules.locationArchitectureConfiguration = locationArchitectureConfiguration;
        InterfaceArchitecturesRules.interfaceArchitectureConfiguration = interfaceArchitectureConfiguration;

        /* Set up all the configuration */
        SetupConfiguration.setUp();
    }

    @ArchTest
    public static final ArchRules namingArchRules = ArchRules.in(NamingArchitectureRules.class);

    @ArchTest
    public static final ArchRules locationArchRules = ArchRules.in(LocationArchitectureRules.class);

    @ArchTest
    public static final ArchRules layerArchRules = ArchRules.in(LayerArchitectureRules.class);

    // @ArchTest
    // public static final ArchRules interfaceArchRules = ArchRules.in(InterfaceArchitecturesRules.class);
}
