package architecture.configuration;

import com.soprasteria.architecture.naming.INamingArchitectureConfiguration;
import com.soprasteria.architecture.naming.NamingArchitectureRules;

public class NamingArchitectureConfiguration implements INamingArchitectureConfiguration {

    @Override
    public boolean freezeIsEnable() {
        return true;
    }
}
