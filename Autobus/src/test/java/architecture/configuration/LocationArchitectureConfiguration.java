package architecture.configuration;

import com.soprasteria.architecture.location.ILocationArchitectureConfiguration;
import com.soprasteria.architecture.location.LocationArchitectureRules;

public class LocationArchitectureConfiguration implements ILocationArchitectureConfiguration {

    @Override
    public void configureRuleClassesNamedShouldResidesInPackageSuffixedExclusivelyBy() {
        LocationArchitectureRules.classesNamedShouldResidesInPackageSuffixedExclusivelyBy.put(".*Action", "..actions..");
        LocationArchitectureRules.classesNamedShouldResidesInPackageSuffixedExclusivelyBy.put(".*Bean", "..beans..");
        LocationArchitectureRules.classesNamedShouldResidesInPackageSuffixedExclusivelyBy.put(".*Service", "..services..");

        /* New line */
        LocationArchitectureRules.classesNamedShouldResidesInPackageSuffixedExclusivelyBy.put(".*Rule", "..rules..");
    }

    @Override
    public boolean freezeIsEnable() {
        return true;
    }
}
