package tec;

interface ICaractereArret {
  static final ICaractereArret ArretCalme = new ArretCalme();
  static final ICaractereArret ArretNerveux = new ArretNerveux();
  static final ICaractereArret ArretAgoraphobe = new ArretAgoraphobe();
  static final ICaractereArret ArretPrudent = new ArretPrudent();
  static final ICaractereArret ArretPoli = new ArretPoli();

      void choixPlaceArret(Vehicule v, int distanceDestination, Passager p);

}
