package tec;

import tec.exceptions.TecException;

public class PassagerStandard implements Usager{

    private MonteeRepos PassagerStand;

    public PassagerStandard(String nom, int destination){
        PassagerStand = new MonteeRepos(nom,destination,ICaractereArret.ArretCalme);
    }

    final public void monterDans(Transport t) throws TecException {
      PassagerStand.monterDans(t);
    }
}
