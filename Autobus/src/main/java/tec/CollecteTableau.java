package tec;
import java.util.*;
public class CollecteTableau implements Collecte {

    ArrayList<int[]> tab = new ArrayList<int[]>();
    private int numArret = 1;
    private int ENTREE = 1;
    private int SORTIE = 2;
    private int indice_tableau = 0;

    @Override
    public void uneEntree(){
        tab.get(indice_tableau)[ENTREE] += 1;
    }

    @Override
    public void uneSortie(){
        tab.get(indice_tableau+1)[SORTIE] += 1;
    }

    @Override
    public void changerArret(){
        numArret += 1;
        indice_tableau += 1;
        this.add_one();
        /*if(tab.length-1<indice_tableau){
            int agrandissement = tab.length*2;
            int[][] copy_to = new int[agrandissement][3];
            System.arraycopy(copy_to,0,tab,0,tab.length*2);
            tab = copy_to;
        }*/
    }


    public void initialise(){
        int[] sous_tableau = new int[3];
        init_tab(sous_tableau);
        tab.add(sous_tableau);
        tab.add(sous_tableau);
    }

    private void init_tab(int[] sous_tableau){
        sous_tableau[0] = numArret;
        sous_tableau[ENTREE] = 0;
        sous_tableau[SORTIE] = 0;
    }

    private void add_one(){
        int[] sous_tableau = new int[3];
        init_tab(sous_tableau);
        tab.add(sous_tableau);
    }

    public String toString() {
        String toret = "";
        ArrayList<int[]> arr = (ArrayList<int[]>) tab.clone();
        arr.remove(0);
        arr.remove(arr.size()-1);
        for(int[] p : arr){
            toret += p[ENTREE]+" utilisateurs sont entrés et " +p[SORTIE] +" utilisateur sont sortis à l'arrêt numéro " + p[0] +"\n";
        }
        return toret;
    }
}
