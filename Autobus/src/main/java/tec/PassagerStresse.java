package tec;

import tec.exceptions.TecException;

public class PassagerStresse implements Usager {

    private MonteeRepos PassagerStress;

    public PassagerStresse(String nom, int destination){
        PassagerStress = new MonteeRepos(nom,destination, ICaractereArret.ArretPrudent);
    }

    final public void monterDans(Transport t) throws TecException {
      PassagerStress.monterDans(t);
    }
}
