/**Cette classe représente une classe abstraite d'Usager
  **/
package tec;

import tec.exceptions.TecException;

public interface Usager{
  /** Cette méthode permet à un usager de monter dans un transport.
  **/
  public void monterDans(Transport t) throws TecException, TecException;
}
