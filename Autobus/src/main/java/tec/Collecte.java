package tec;

public interface Collecte {

    void uneEntree();

    void uneSortie();

    void changerArret();

    void initialise();

}
