#!/bin/bash

red=`tput setaf 1`
green=`tput setaf 2`
reset=`tput sgr0`

# Checking all the arguments are there
if [ "$#" -ne 3 ]; then
    echo "Missing arguments : IP PORT JSON_FILE";
    exit 1;
fi

echo "[Sending $3 to $1]";
if test -f $3; then
    echo "File $3 found.";
else
    echo "File $3 not found.";
    exit 1;
fi


# Sending data to API
STATUSCODE=$(curl http://$1:$2/api/e_search/create/ -X POST -H "Content-Type: application/json" -d @$3 --output /dev/stderr --write-out "%{http_code}")

if test $STATUSCODE -ne 201; then
    # error handling
    # …(failure)
    echo -e "\n$STATUSCODE";
    echo -e "\n${red}Operation not successful${reset}";
    exit 1;
else
    # …(success)
    echo -e "\n${green}Data successfully sent${reset}";
    exit 0;
fi;


    
